export function setUserInfo(state, payload) {
  state.userInfo = payload;
}

export function setAvailableTokens(state, payload) {
  state.availableTokens = payload;
}
